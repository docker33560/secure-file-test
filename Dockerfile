FROM alpine:3.16.2
RUN apk --no-cache add ca-certificates curl bash 
COPY myscript.sh /usr/local/bin
ENTRYPOINT ["/usr/local/bin/myscript.sh"]
